package INF102.lab4.median;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class QuickMedian implements IMedian {

    static Random rng = new Random();

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list

        int middleOfList = list.size() / 2;

        return quickSelect(list, middleOfList, list.size() -1, 0);
    }
    private <T extends Comparable<T>> T quickSelect(List<T> list, int middleOfList, Integer highIndex, Integer lowIndex) {

        int smallerCount = partition(list, highIndex, lowIndex);

        if (smallerCount == middleOfList) {
            return list.get(highIndex);
        }
        else{
            if( smallerCount < middleOfList){
                lowIndex = smallerCount;

            }
            else{
                highIndex = smallerCount - 1;
            }
            return quickSelect(list, middleOfList, highIndex, lowIndex);
        }
    }

    public <T extends Comparable<T>> int partition(List<T> list, Integer highIndex, Integer lowIndex) {

        swap(rng.nextInt(lowIndex, highIndex +1),highIndex, list);
        T pivot =  list.get(highIndex);

        int counter = lowIndex;

        for(int i = lowIndex; i < highIndex; i++){
            T pointer = list.get(i);
            if(pointer.compareTo(pivot) < 0){
                swap(counter, i,list);
                counter++;
            }
        }
        return counter;
    }
    private <T extends Comparable<T>> void swap(Integer counter, Integer i, List<T> list) {
        Collections.swap(list, counter, i);
    }
}



