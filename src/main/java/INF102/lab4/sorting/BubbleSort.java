package INF102.lab4.sorting;

import java.util.Collections;
import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> List sort(List<T> list) {
        boolean swapped;
        int n = list.size();

        for(int i = 0; i < n; i++){
            swapped = false;

            for(int j = i +1; j < n; j++){

                if(list.get(i).compareTo(list.get(j)) > 0){
                    Collections.swap(list, i, j);
                    swapped = true;
                }
            }
            if(swapped){
                sort(list);
            }
            if(!swapped){
                break;
            }
        }
        return list;
    }
}

    

